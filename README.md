競プロ典型 90 問（[GitHub]、[AtCoder]）に対する私個人の解答です。

大半の解答は Ruby で書かれています。以前はよく使っていたのですが、最近は本業でも趣味でも使うことがなくなってしまったので、あえて Ruby を使っています。

提出したプログラムは `sol/` ディレクトリの中にあります。

想定解以外の方法で解いた問題のうち、一部の問題については [Qiita] に詳しい解説を載せています。結構時間をかけて書いているので、読んでいただけるとうれしいです。

[GitHub]: https://github.com/E869120/kyopro_educational_90/
[AtCoder]: https://atcoder.jp/contests/typical90
[Qiita]: https://qiita.com/yuizumi/items/7e72fa9600f8059ae1c4
