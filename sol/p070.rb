n = readline.to_i

x = Array.new(n)
y = Array.new(n)
for i in 0...n
    x[i], y[i] = readline.split.map(&:to_i)
end

x.sort!; xx = x[n / 2]
y.sort!; yy = y[n / 2]

ans = 0

for i in 0...n
    ans += (x[i] - xx).abs + (y[i] - yy).abs
end

puts ans