#include <algorithm>
#include <cstdio>
#include <map>

using namespace std;

int main()
{
    int w, n; scanf("%d%d", &w, &n);
    map<int, int> m;
    m.emplace(0, 0);

    for (int i = 0; i < n; i++) {
        int l, r; scanf("%d%d", &l, &r);
        ++r;

        auto head = m.upper_bound(l);
        --head;
        int next_h = head->second;

        auto tail = head;
        int tail_h = -1;
        while (tail != m.end() && tail->first < r) {
            tail_h = tail->second;
            next_h = max(next_h, tail_h);
            ++tail;
        }

        ++next_h;

        if (head->first < l)
            ++head;
        if (tail->first == r) {
            m.erase(head, tail);
            m.emplace(l, next_h);
        } else {
            m.erase(head, tail);
            m.emplace(l, next_h);
            m.emplace(r, tail_h);
        }

        printf("%d\n", next_h);
    }

    return 0;
}
