def lis(n, a)
    tmp = [a[0]]
    out = [1]
    for i in 1...n
        j = tmp.bsearch_index { |x| x >= a[i] }
        tmp[j || tmp.size] = a[i]
        out.push(tmp.size)
    end
    out
end

def main
    n = readline.to_i
    a = readline.split.map(&:to_i)

    fw = lis(n, a)
    a.reverse!
    bw = lis(n, a)
    bw.reverse!

    puts fw.zip(bw).map { |f, b| f + b - 1 }.max
end

main if __FILE__ == $0
