using System;

static class P081
{
    private const int K = 5000;

    static void Main()
    {
        string[] nk = Console.ReadLine().Split();
        int n = Int32.Parse(nk[0]);
        int k = Int32.Parse(nk[1]);

        int[,] sum = new int[K+1, K+1];

        for (int i = 0; i < n; i++)
        {
            string[] ab = Console.ReadLine().Split();
            int a = Int32.Parse(ab[0]);
            int b = Int32.Parse(ab[1]);
            ++sum[a, b];
        }

        for (int a = 1; a <= K; a++)
        for (int b = 0; b <= K; b++)
            sum[a, b] += sum[a - 1, b];
        for (int a = 0; a <= K; a++)
        for (int b = 1; b <= K; b++)
            sum[a, b] += sum[a, b - 1];

        int ans = 0;

        for (int a = 0; a <= K; a++)
        for (int b = 0; b <= K; b++)
        {
            int a0 = Math.Max(0, a - k - 1);
            int b0 = Math.Max(0, b - k - 1);
            ans = Math.Max(ans, sum[a, b] - sum[a0, b] - sum[a, b0] + sum[a0, b0]);
        }

        Console.WriteLine(ans);
    }
}
