n = readline.to_i

exp = [0.0] * 101
ans = 0
n.times do
    l, r = readline.split.map(&:to_i)
    prob = 1.0 / (r - l + 1)
    sum = 0
    100.downto(l) do |i|
        ans += sum * prob if l <= i && i <= r
        sum += exp[i]
        exp[i] += prob if l <= i && i <= r
    end
end

puts ans
