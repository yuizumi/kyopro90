M = 10 ** 9 + 7

Query = Struct.new(:x, :y, :z, :w)

n, q = readline.split.map(&:to_i)
queries = Array.new(q) do
    x, y, z, w = readline.split.map(&:to_i)
    Query.new(x - 1, y - 1, z - 1, w)
end

ans = 1

for k in 0...60
    count = 0
    for m in 0...(1 << n)
        if queries.all? { |qi| m[qi.x] | m[qi.y] | m[qi.z] == qi.w[k] } then
            count += 1
        end
    end
    ans = (ans * count) % M
end

puts ans
