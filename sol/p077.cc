#include <cstdio>
#include <cmath>
#include <atcoder/maxflow>

using namespace std;
using namespace atcoder;

int main()
{
    int n, t; scanf("%d%d", &n, &t);

    mf_graph<int> graph(2 * n + 2);
    const int src = 2 * n;
    const int dst = src + 1;

    vector<int> ax(n), ay(n);
    vector<int> bx(n), by(n);

    for (int i = 0; i < n; i++) {
        scanf("%d%d", &ax[i], &ay[i]);
        graph.add_edge(src, i + 0, 1);
    }
    for (int i = 0; i < n; i++) {
        scanf("%d%d", &bx[i], &by[i]);
        graph.add_edge(i + n, dst, 1);
    }
    for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++) {
        const int dx = bx[j] - ax[i];
        const int dy = by[j] - ay[i];
        if (!(dx == -t || dx == 0 || dx == +t)) continue;
        if (!(dy == -t || dy == 0 || dy == +t)) continue;
        if (dx == 0 && dy == 0) continue;
        graph.add_edge(i + 0, j + n, 1);
    }
    const int flow = graph.flow(src, dst);
    if (flow != n) {
        puts("No");
        return 0;
    }

    puts("Yes");

    vector<int> d(n, 0);
    for (const auto& e : graph.edges()) {
        if (e.from == src || e.to == dst || e.flow == 0)
            continue;
        const int dx = bx[e.to - n] - ax[e.from];
        const int dy = by[e.to - n] - ay[e.from];
        const int angle = static_cast<int>(round(atan2(dy, dx) / M_PI * 4.0));
        d[e.from] = (angle + 8) % 8 + 1;
    }
    for (int i = 0; i < n; i++) {
        printf("%d%s", d[i], (i == n - 1) ? "\n" : " ");
    }

    return 0;
}
