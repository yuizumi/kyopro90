n, k = readline.split.map(&:to_i)
scores = []
n.times do
    a, b = readline.split.map(&:to_i)
    scores.push(a - b)
    scores.push(b)
end

scores.sort!.reverse!
puts scores[0...k].inject(0, &:+)
