def solve(n, a, b, c)
    a, b, c = [a, b, c].sort!
    ans = 9999
    (n / c).downto(0) do |kc|
        m = n - c * kc
        (m / b).downto(0) do |kb|
            l = m - b * kb
            if l % a == 0 then
                ans = [ans, (l / a) + kb + kc].min
            end
        end
    end
    return ans
end
            
def main
    n = readline.to_i
    a, b, c = readline.split.map(&:to_i)
    puts solve(n, a, b, c)
end

main if __FILE__ == $0
