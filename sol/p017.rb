class OnlineSum
    def initialize(n)
        @n = 2 ** Math.log2(n).ceil
        @data = [0] * @n
    end

    def inc(i)
        m = 1
        while m <= @n
            @data[i] += 1 if (i & m) == 0
            i |= m; m <<= 1
        end
    end

    def sum(i)
        i += 1
        m = 1
        acc = 0
        while i > 0
            acc += @data[i - 1] if (i & m) != 0
            i &= ~m; m <<= 1
        end
        acc
    end
end

def main
    n, m = readline.split.map(&:to_i)

    graph = Array.new(n) { [] }
    m.times do
        a, b = readline.split.map(&:to_i)
        graph[a - 1].push(b - 1)
    end
    os = OnlineSum.new(n)
    ans = 0
    for u in 0...n
        graph[u].sort!.reverse!
        for v in graph[u]
            ans += os.sum(v - 1) - os.sum(u)
            os.inc(v)
        end
    end
    puts ans
end

main if __FILE__ == $0
