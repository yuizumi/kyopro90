#include <algorithm>
#include <array>
#include <iostream>
#include <vector>

#include <atcoder/modint>

using namespace std;

using mint = ::atcoder::modint998244353;

//------------------------
//  Matrix

template <typename T, size_t M, size_t N>
using Matrix = array<array<T, N>, M>;

template <typename T, size_t L, size_t M, size_t N>
Matrix<T, L, N> operator*(const Matrix<T, L, M>& a,
                          const Matrix<T, M, N>& b)
{
    Matrix<T, L, N> c;

    for (int i = 0; i < L; i++)
    for (int j = 0; j < N; j++) {
        c[i][j] = 0;
        for (int k = 0; k < M; k++) c[i][j] += a[i][k] * b[k][j];
    }

    return c;
}

//------------------------
//  Solution

mint solve2(long n, int k)
{
    Matrix<mint, 2, 2> x = {{{0, 1}, {1, 1}}};
    Matrix<mint, 2, 2> y = {{{1, 0}, {0, 1}}};

    for (long e = n + 1; e > 0; e >>= 1) {
        if (e & 1) y = y * x;
        x = x * x;
    }

    return y[1][1];
}

mint solve5(long n, int k)
{
    vector<int> vs = {0};

    for (int i = 1; i * i <= k; i++) {
        vs.push_back(i);
        if (i * i < k) vs.push_back(k / i);
    }
    sort(vs.begin(), vs.end());
    vs.push_back(INT_MAX);

    const int m = vs.size();

    vector<vector<mint>> dp(n + 1, vector<mint>(m));
    dp[0][m-1] = 1;

    for (int i = 1; i <= n; i++) {
        for (int j = 0; j < m; j++) {
            if (vs[j] > k / i) {
                dp[i][j] = dp[i][j-1];
            } else {
                for (int h = 1; h <= i; h++) {
                    dp[i][j] += (dp[h-1][m-1] - dp[h-1][j]) * (vs[j] + 1) * dp[i-h][m-1];
                }
            }
        }
    }

    return dp[n][m-1];
}

mint solve(long n, int k)
{
    if (n <= 1000000000000 && k == 1)
        return solve2(n, k);

    if (n <= 10000 && k <= 10000)
        return solve5(n, k);

    return 0;
}

int main()
{
    long n; int k;
    cin >> n >> k;
    cout << solve(n, k).val() << endl;
    return 0;
}
