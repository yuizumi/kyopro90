FIB = [1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987]

def query(i)
    puts "? #{i}"
    $stdout.flush
    readline.to_i
end

def doit
    n = readline.to_i
    offset = 0
    a = []
    (FIB.size - 2).downto(0) do |i|
        lo = offset + FIB[i + 0]
        hi = offset + FIB[i + 1]
        a[lo] ||= (lo <= n) ? query(lo) : 0
        a[hi] ||= (hi <= n) ? query(hi) : 0
        offset = lo if a[lo] < a[hi]
    end
    puts "! #{a[offset + 1]}"
    $stdout.flush
end

def main
    t = readline.to_i
    t.times { doit }
end

main if __FILE__ == $0
