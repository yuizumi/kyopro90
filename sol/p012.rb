class Node
    def initialize
        @parent = nil
        @size = 1
    end

    def root
        if @parent then
            @parent = @parent.root
        else
            self
        end
    end

    def union(other)
        a = self.root
        b = other.root
        return if a == b

        if a.size >= b.size then
            b.parent = a
            a.size += b.size
        else
            a.parent = b
            b.size += a.size
        end
    end

    attr_accessor :parent, :size
end

def main
    h, w = readline.split.map(&:to_i)

    grid = Array.new(h+2) { [nil] * (w + 2) }

    readline.to_i.times do
        q = readline.split.map(&:to_i)
        if q[0] == 1 then
            _, y, x = q
            grid[y][x] = node = Node.new
            grid[y+1][x].union(node) if grid[y+1][x]
            grid[y][x+1].union(node) if grid[y][x+1]
            grid[y-1][x].union(node) if grid[y-1][x]
            grid[y][x-1].union(node) if grid[y][x-1]
        else
            _, y1, x1, y2, x2 = q
            g1 = grid[y1][x1]
            g2 = grid[y2][x2]
            if g1 && g2 && g1.root == g2.root then
                puts 'Yes'
            else
                puts 'No'
            end
        end
    end
end

main if __FILE__ == $0
