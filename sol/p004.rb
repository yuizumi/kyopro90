lines = readlines

h, w = lines.shift.split.map(&:to_i)

a = Array.new(h) do |i|
    lines[i].split.map(&:to_i)
end

cols = a.transpose.map(&:sum)

for i in 0...h
    a_i = a[i]
    row = a_i.sum
    for j in 0...w
        a_i[j] = (row + cols[j] - a_i[j]).to_s
    end
    puts a_i.join(' ')
end
