using System;
using System.Linq;

static class P007
{
    static void Main()
    {
        int n = Int32.Parse(Console.ReadLine());

        int[] a = Console.ReadLine().Split().Select(Int32.Parse)
            .ToArray();
        Array.Sort(a);
        
        int q = Int32.Parse(Console.ReadLine());
        while (--q >= 0)
        {
            int b_i = Int32.Parse(Console.ReadLine());

            int j = Array.BinarySearch(a, b_i);
            if (j >= 0)
            {
                Console.WriteLine(0);
            }
            else
            {
                int ans = Int32.MaxValue;
                if (~j > 0) ans = Math.Min(ans, b_i - a[~j - 1]);
                if (~j < n) ans = Math.Min(ans, a[~j - 0] - b_i);
                Console.WriteLine(ans);
            }
        }
    }
}
