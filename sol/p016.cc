#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

int solve(int n, int a, int b, int c)
{
    vector<int> v = {a, b, c};
    sort(v.begin(), v.end());
    a = v[0], b = v[1], c = v[2];
    int ans = 9999;
    for (int kc = n / c; kc >= 0; --kc) {
        int m = n - c * kc;
        for (int kb = m / b; kb >= 0; --kb) {
            int l = m - b * kb;
            if (l % a == 0) {
                ans = min(ans, l / a + kb + kc);
            }
        }
    }
    return ans;
}

int main()
{
    int n, a, b, c;
    cin >> n >> a >> b >> c;
    cout << solve(n, a, b, c) << endl;
    return 0;
}
