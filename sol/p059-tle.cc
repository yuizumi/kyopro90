#include <algorithm>
#include <cstdio>
#include <queue>
#include <utility>
#include <vector>

using namespace std;

using Graph = vector<vector<int>>;

int main()
{
    int n, m, q; scanf("%d%d%d", &n, &m, &q);

    Graph fw(n), bw(n);

    for (int i = 0; i < m; i++) {
        int x, y; scanf("%d%d", &x, &y); --x, --y;
        fw[x].push_back(y);
        bw[y].push_back(x);
    }
    for (int i = 0; i < n; i++) {
        sort(fw[i].begin(), fw[i].end());
        const auto fw_end = unique(fw[i].begin(), fw[i].end());
        fw[i].erase(fw_end, fw[i].end());
        sort(bw[i].begin(), bw[i].end());
        const auto bw_end = unique(bw[i].begin(), bw[i].end());
        bw[i].erase(bw_end, bw[i].end());
    }

    vector<int> map(n);
    for (int i = 0; i < n; i++) map[i] = i;

    bool updated = true;
    while (updated) {
        updated = false;
        for (int i = 0; i < n; i++) {
            const int j = map[i];
            if (fw[j].size() == 1 && bw[fw[j][0]].size() == 1) {
                const int fw_0 = fw[j][0];
                map[fw_0] = j;
                fw[j] = move(fw[fw_0]);
                fw[fw_0].clear();
                updated = true;
            }
        }
    }

    while (--q >= 0) {
        int a, b; scanf("%d%d", &a, &b); --a, --b;
        a = map[a], b = map[b];

        vector<int> vis(n, 0);
        vis[a] = 1;
        queue<int> cue;
        cue.push(a);
        while (!cue.empty()) {
            const int u = cue.front();
            cue.pop();
            for (const int v : fw[u]) {
                if (vis[v] != 0) continue;
                cue.push(v);
                vis[v] = 1;
                if (v == b) goto done;
            }
        }
    done:
        printf(vis[b] ? "Yes\n" : "No\n");
    }

    return 0;
}
