M = 10 ** 9 + 7

n, l = readline.split.map(&:to_i)
dp = [1]
for i in 1..n
    dp[i] = dp[i-1]
    dp[i] = (dp[i] + dp[i-l]) % M if i >= l
end
puts dp[n]
