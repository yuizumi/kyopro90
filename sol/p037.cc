#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

class Rmq
{
public:
    explicit Rmq(int n) : n_(1)
    {
        while (n_ < n) n_ *= 2;
        a_.resize(2 * n_);
    }

    long lookup(int lo, int hi) const
    {
        return lookup(lo, hi, 1, 0, n_);
    }

    void update(int i, long value)
    {
        for (i += n_; i > 0; i /= 2) a_[i] = max(a_[i], value);
    }

private:
    long lookup(int lo, int hi, int i, int lo_i, int hi_i) const
    {
        if (hi <= lo_i || hi_i <= lo) {
            return 0;
        }
        if (lo <= lo_i && hi_i <= hi) {
            return a_[i];
        }
        int md_i = (lo_i + hi_i) / 2;
        const long lower = lookup(lo, hi, 2 * i + 0, lo_i, md_i);
        const long upper = lookup(lo, hi, 2 * i + 1, md_i, hi_i);
        return max(lower, upper);
    }

    int n_;
    vector<long> a_;
};

int main()
{
    int w, n; cin >> w >> n;

    Rmq rmq(w + 1);
    rmq.update(0, 1);
    while (--n >= 0) {
        int l, r; long v; cin >> l >> r >> v;

        for (int i = w; i >= r; i--) {
            const long q = rmq.lookup(i - r, i - l + 1);
            if (q > 0) rmq.update(i, q + v);
        }
        for (int i = r; i >= l; i--) {
            const long q = rmq.lookup(0    , i - l + 1);
            if (q > 0) rmq.update(i, q + v);
        }
    }

    cout << rmq.lookup(w, w + 1) - 1 << endl;

    return 0;
}
