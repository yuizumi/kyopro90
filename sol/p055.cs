using System;
using System.Linq;

static class P055
{
    static void Main()
    {
        string[] npq = Console.ReadLine().Split();
        int  n = Int32.Parse(npq[0]);
        long p = Int64.Parse(npq[1]);
        long q = Int64.Parse(npq[2]);

        long[] a = Console.ReadLine().Split().Select(Int64.Parse).ToArray();

        int count = 0;
        int k0 = n;
        for (int k1 = 0; k1 < k0; ++k1)
        for (int k2 = 0; k2 < k1; ++k2)
        for (int k3 = 0; k3 < k2; ++k3)
        for (int k4 = 0; k4 < k3; ++k4)
        for (int k5 = 0; k5 < k4; ++k5) {
            if (a[k1] * a[k2] % p * a[k3] % p * a[k4] % p * a[k5] % p == q)
            {
                ++count;
            }
        }

        Console.WriteLine(count);
    }
}
