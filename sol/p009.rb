EPS = 1e-8

n = readline.to_i
z = Array.new(n) do
    x, y = readline.split.map(&:to_i)
    Complex(x, y)
end

ans = 0

for z_i in z
    angles = z.reject { |z_j| z_j == z_i }.map do |z_j|
        (z_j - z_i).angle
    end
    angles.sort!

    max = angles[0]
    max_i = 0
    for min in angles
        while max - min < Math::PI + EPS do
            ans = max - min if ans < max - min
            max_i += 1
            if max_i < angles.size then
                max = angles[max_i]
            else
                max = angles[max_i % angles.size] + 2 * Math::PI
            end
        end
    end
end

puts ans / Math::PI * 180.0
