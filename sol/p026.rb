def traverse(graph, u, group)
    for v in graph[u]
        if not group[v] then
            group[v] = 1 - group[u]
            traverse(graph, v, group)
        end
    end
end

def main
    n = readline.to_i

    graph = Array.new(n + 1) { [] }
    (n - 1).times do
        a, b = readline.split.map(&:to_i)
        graph[a].push(b)
        graph[b].push(a)
    end

    group = [nil, 1]
    traverse(graph, 1, group)

    out = (group.count(0) >= n / 2) ? 0 : 1
    count = 0
    puts (
        group.each_with_index.select do |g, i|
            g == out
        end
    ).take(n / 2).map {|g, i| i}.join(' ')
end

main if __FILE__ == $0
