#include <iostream>
#include <utility>
#include <vector>

using namespace std;

constexpr long M = 1000000007;

int main()
{
    long n, b, k; cin >> n >> b >> k;

    vector<long> x(b);
    for (int i = 0; i < k; i++) {
        int ck; cin >> ck;
        ++x[ck % b];
    }
    vector<long> y(b);
    y[0] = 1;
    vector<int>  σ(b);
    for (int i = 0; i < b; i++) {
        σ[i] = (i * 10) % b;
    }

    for (; n > 0; n /= 2) {
        if (n % 2 != 0) {
            vector<long> yx(b);
            for (int i = 0; i < b; i++) {
                for (int j = 0; j < b; j++) {
                    const int k = (σ[i] + j) % b;
                    yx[k] = (yx[k] + y[i] * x[j]) % M;
                }
            }
            y = move(yx);
        }
        {
            vector<long> xx(b);
            for (int i = 0; i < b; i++) {
                for (int j = 0; j < b; j++) {
                    const int k = (σ[i] + j) % b;
                    xx[k] = (xx[k] + x[i] * x[j]) % M;
                }
            }
            x = move(xx);
        }
        {
            vector<int> σσ(b);
            for (int i = 0; i < b; i++) σσ[i] = σ[σ[i]];
            σ = move(σσ);
        }
    }

    cout << y[0] << endl;
    return 0;
}
