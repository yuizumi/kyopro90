// Note: 解説を見てから解いた

#include <algorithm>
#include <bitset>
#include <cstdio>
#include <queue>
#include <utility>
#include <vector>

using namespace std;

constexpr int Q = 1000;

int main()
{
    int n, m, q; scanf("%d%d%d", &n, &m, &q);

    vector<vector<int>> g(n);
    vector<int> a(q), b(q);

    for (int i = 0; i < m; i++) {
        int x, y; scanf("%d%d", &x, &y);
        g[--x].push_back(--y);
    }

    for (int i = 0; i < q; i++) {
        scanf("%d%d", &a[i], &b[i]), --a[i], --b[i];
    }

    for (int i = 0; i < q; i += Q) {
        vector<bitset<Q>> dp(n);
        for (int j = 0; j < Q && i + j < q; j++) {
            dp[a[i+j]].set(j);
        }
        for (int j = 0; j < n; j++) {
            for (const int k : g[j]) dp[k] |= dp[j];
        }
        for (int j = 0; j < Q && i + j < q; j++) {
            printf("%s\n", dp[b[i+j]][j] ? "Yes" : "No");
        }
    }

    return 0;
}
