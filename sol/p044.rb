n, q = readline.split.map(&:to_i)
a = readline.split.map(&:to_i)

i = 0

q.times do
    t, x, y = readline.split.map(&:to_i)
    x = (x - 1 + i) % n
    y = (y - 1 + i) % n
    case t
    when 1
        a[x], a[y] = a[y], a[x]
    when 2
        i = (i + n - 1) % n
    when 3
        puts a[x]
    end
end
