t = readline.to_i
l, sx, sy = readline.split.map(&:to_i)
q = readline.to_i

π = Math::PI

q.times do
    ei = readline.to_i
    θ = 2 * π * ei / t
    wz = 0.5 * l * (1 - Math.cos(θ))
    wy = 0.5 * l * (0 - Math.sin(θ))
    φ = Math.atan2(wz, Math.hypot(sx, sy - wy))
    puts φ / π * 180.0
end
