using System;
     
public struct IntM : IEquatable<IntM>
{
    private const long M = 1000000007;

    private IntM(long value)
    {
        Value = value % M;
    }

    public long Value { get; }

    public static implicit operator IntM(long value)
    {
        return (value >= 0) ? new IntM(value) : new IntM(M + value % M);
    }

    public static IntM operator-(IntM x) => new IntM(M - x.Value);
     
    public static IntM operator+(IntM x, IntM y) => new IntM(x.Value + y.Value);
    public static IntM operator-(IntM x, IntM y) => x + (-y);
    public static IntM operator*(IntM x, IntM y) => new IntM(x.Value * y.Value);

    public static IntM operator/(IntM a, IntM b)
    {
        ExtGcd(M, b.Value, out long x, out long y);
        return a * y;
    }

    private static void ExtGcd(long a, long b, out long x, out long y)
    {
        if (b == 0)
        {
            x = 1;
            y = 0;
        }
        else
        {
            ExtGcd(b, a % b, out y, out x);
            y -= (a / b) * x;
        }
    }

    public static bool operator==(IntM x, IntM y) =>  x.Equals(y);
    public static bool operator!=(IntM x, IntM y) => !x.Equals(y);

    public bool Equals(IntM that) => this.Value == that.Value;

    public override bool Equals(object obj) => (obj is IntM that) && Equals(that);
    public override int GetHashCode() => Value.GetHashCode();

    public override string ToString() => Value.ToString();
}

static class P015
{
    static void Main()
    {
        int n = Int32.Parse(Console.ReadLine());

        var fact = new IntM[n + 1];
        fact[0] = 1;
        for (int i = 1; i <= n; i++)
        {
            fact[i] = fact[i - 1] * i;
        }
        for (int k = 1; k <= n; k++)
        {
            IntM ans = 0;
            for (int j = 1; j <= n; j++)
            {
                int nn = n - 1 - (j - 1) * k;
                if (nn < 0) break;
                int rr = j + 1;
                ans += fact[nn + rr - 1] / (fact[nn] * fact[rr - 1]);
            }
            Console.WriteLine(ans);
        }
    }
}
