M = 10**9 + 7

readline  # N
s = readline

dp = [1] + [0] * 7
s.each_char do |s_i|
    dp[1] = (dp[0] + dp[1]) % M if s_i == 'a'
    dp[2] = (dp[1] + dp[2]) % M if s_i == 't'
    dp[3] = (dp[2] + dp[3]) % M if s_i == 'c'
    dp[4] = (dp[3] + dp[4]) % M if s_i == 'o'
    dp[5] = (dp[4] + dp[5]) % M if s_i == 'd'
    dp[6] = (dp[5] + dp[6]) % M if s_i == 'e'
    dp[7] = (dp[6] + dp[7]) % M if s_i == 'r'
end

puts dp[7]
