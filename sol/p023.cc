#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

constexpr int M = 1000000007;

class TransMap
{
public:
    explicit TransMap(int w)
    {
        unordered_map<int, vector<int>> trans_map;

        init_trans_map(trans_map, 0, 0, 1 << (w - 1));

        state_vec_ = trans_map[0];
        trans_map_.resize(state_vec_.size());

        unordered_map<int, int> state_map(state_vec_.size());
        for (int id = 0; id < state_vec_.size(); id++)
            state_map.emplace(state_vec_[id], id);

        for (auto& [curr, next_vec] : trans_map) {
            for (int& next : next_vec) next = state_map[next];
            trans_map_[state_map[curr]] = move(next_vec);
        }
    }

    size_t size() const { return state_vec_.size(); }

    const vector<int>& next(int id) const { return trans_map_[id]; }
    int state(int id) const { return state_vec_[id]; }

private:
    void init_trans_map(unordered_map<int, vector<int>>& trans_map,
                        int upper, int lower, int index)
    {
        if (index == 0) {
            trans_map[upper].push_back(lower);
        } else {
            init_trans_map(trans_map, upper, lower, index >> 1);
            init_trans_map(trans_map, upper, lower | index, index >> 2);
            init_trans_map(trans_map, upper | index, lower, index >> 2);
        }
    }

    vector<int> state_vec_;
    vector<vector<int>> trans_map_;
};
    
int main()
{
    int h, w; cin >> h >> w;
    TransMap map(w);

    vector<int> dp0(map.size());
    vector<int> dp1(map.size());
    dp0[0] = 1;

    for (int y = 0; y < h; y++) {
        string s; cin >> s;
        int wall = 0;
        for (int x = 0; x < w; x++) {
            wall <<= 1;
            if (s[x] == '#') wall |= 1;
        }

        fill(dp1.begin(), dp1.end(), 0);

        for (int i = 0; i < map.size(); ++i) {
            for (const int j : map.next(i)) {
                if ((map.state(j) & wall) == 0) dp1[j] = (dp1[j] + dp0[i]) % M;
            }
        }

        swap(dp0, dp1);
    }

    int sum = 0;
    for (int i = 0; i < map.size(); ++i)
        sum = (sum + dp0[i]) % M;
    cout << sum << endl;

    return 0;
}
