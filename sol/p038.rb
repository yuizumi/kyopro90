MAX = 10 ** 18

a, b = readline.split.map(&:to_i)
lcm = a.lcm(b)
puts (lcm <= MAX) ? lcm : 'Large'
