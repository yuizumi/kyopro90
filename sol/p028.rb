imos = Array.new(1001) { [0] * 1001 }

n = readline.to_i
n.times do
    x1, y1, x2, y2 = readline.split.map(&:to_i)
    imos[x1][y1] += 1; imos[x2][y1] -= 1
    imos[x1][y2] -= 1; imos[x2][y2] += 1
end

for y in 0...1000
    for x in 1...1000
        imos[x][y] += imos[x - 1][y]
    end
end

freq = [0] * (n + 1)
for x in 0...1000
    height = 0
    for y in 0...1000
        height += imos[x][y]; freq[height] += 1
    end
end

puts freq[1..n]
