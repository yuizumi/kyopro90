n = readline.to_i
a = Array.new(n) do
    readline.split.map(&:to_i)
end

b = Array.new(n) { [] }
m = readline.to_i
m.times do
    x, y = readline.split.map { |s| s.to_i - 1 }
    b[x][y] = b[y][x] = true
end

ans = -1
(0...n).to_a.permutation do |p|
    sum = a[p[0]][0]
    for j in 1...n
        if b[p[j-1]][p[j]] then
            sum = -1
            break
        end
        sum += a[p[j]][j]
    end
    if (sum >= 0) && (ans < 0 || ans > sum) then
        ans = sum
    end
end

puts ans
