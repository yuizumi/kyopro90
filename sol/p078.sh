tail -n +2 | awk '{ print ($1 > $2) ? $1 : $2 }' | sort -n | uniq -c | fgrep ' 1 ' | wc -l | tr -d ' '
