#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

vector<vector<long>> compute(const vector<long>& a)
{
    const int n = a.size();

    vector<vector<long>> r(n + 1);

    for (int mask = 0; mask < (1 << n); ++mask) {
        long sum = 0;
        int num = 0;
        for (int i = 0; i < n; i++) {
            if ((mask & (1 << i)) != 0) { sum += a[i], ++num; }
        }
        r[num].push_back(sum);
    }

    for (int i = 0; i <= n; i++) sort(r[i].begin(), r[i].end());
    return r;
}

int main()
{
    int n, k; long p; cin >> n >> k >> p;

    vector<long> a(n);
    for (int i = 0; i < n; i++) cin >> a[i];

    if (n <= 20) {
        const auto r = compute(a);
        cout << upper_bound(r[k].begin(), r[k].end(), p) - r[k].begin() << endl;
    } else {
        const auto r1 = compute({a.begin(), a.begin() + 20});
        const auto r2 = compute({a.begin() + 20, a.end()});

        long ans = 0;
        for (int i = max(0, k - 20); i <= min(k, n - 20); i++) {
            for (const long q : r2[i]) {
                ans += upper_bound(r1[k-i].begin(), r1[k-i].end(), p - q) - r1[k-i].begin();
            }
        }
        cout << ans << endl;
    }

    return 0;
}
