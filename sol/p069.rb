M = 10 ** 9 + 7

n, k = readline.split.map(&:to_i)

if n == 1 then
    puts k
else
    puts k * (k - 1) * (k - 2).pow(n - 2, M) % M
end
