#include <algorithm>
#include <cstdio>
#include <utility>
#include <vector>

using namespace std;

using Graph = vector<vector<int>>;

struct NodeInfo {
    int c_self = 1, t_self = -1;
    int c_prop = 1, t_prop = -1;
};

int main()
{
    int n, m; scanf("%d%d", &n, &m);

    Graph g0(n);
    for (int i = 0; i < m; i++) {
        int a, b; scanf("%d%d", &a, &b);
        g0[a - 1].push_back(b - 1);
        g0[b - 1].push_back(a - 1);
    }

    Graph g1(n);
    for (int u = 0; u < n; u++) {
        for (int v : g0[u]) {
            if (make_pair(g0[u].size(), u) < make_pair(g0[v].size(), v))
                g1[u].push_back(v);
        }
    }

    int q; scanf("%d", &q);
    vector<NodeInfo> info(n);

    for (int t = 0; t < q; t++) {
        int x, y; scanf("%d%d", &x, &y);
        const int u = x - 1;
        for (const int v : g1[u]) {
            if (info[u].t_self < info[v].t_prop) {
                info[u].c_self = info[v].c_prop;
                info[u].t_self = info[v].t_prop;
            }
        }
        printf("%d\n", info[u].c_self);
        for (const int v : g1[u]) {
            info[v].c_self = y;
            info[v].t_self = t;
        }
        info[u].c_self = info[u].c_prop = y;
        info[u].t_self = info[u].t_prop = t;
    }

    return 0;
}
