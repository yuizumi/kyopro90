using System;
using System.Collections.Generic;
using System.Linq;

internal class Node
{
    internal Node(char c)
    {
        Label = (c == 'a') ? A : B;
    }

    private const long M = 1000000007;
    private const int A = 1;
    private const int B = 2;

    internal int Label { get; }

    internal List<Node> Subnodes { get; } = new List<Node>();
    internal Node Parent { get; private set; }

    internal long[] Memo { get; private set; } = new long[4];

    internal void SetParent()
    {
        foreach (Node subnode in Subnodes)
        {
            subnode.Subnodes.Remove(this);
            subnode.Parent = this;
            subnode.SetParent();
        }
    }

    internal void Solve()
    {
        var memo = new long[4];

        Memo[Label] = 1;

        foreach (Node subnode in Subnodes)
        {
            subnode.Solve();

            for (int i = 1; i <= 3; i++)
            {
                for (int j = 1; j <= 3; j++)
                {
                    memo[i | j] = (memo[i | j] + Memo[i] * subnode.Memo[j]) % M;
                }
                memo[i] = (memo[i] + Memo[i] * subnode.Memo[3]) % M;
            }

            var tmp = Memo; Memo = memo; memo = tmp;
            Array.Clear(memo, 0, memo.Length);
        }
    }
}

static class P073
{
    static void Main()
    {
        int n = Int32.Parse(Console.ReadLine());
        Node[] nodes = new Node[n];

        string line = Console.ReadLine();
        for (int i = 0; i < n; i++)
            nodes[i] = new Node(line[i * 2]);

        for (int i = 0; i < n - 1; i++)
        {
            string[] ab = Console.ReadLine().Split();
            int a = Int32.Parse(ab[0]);
            int b = Int32.Parse(ab[1]);
            nodes[a - 1].Subnodes.Add(nodes[b - 1]);
            nodes[b - 1].Subnodes.Add(nodes[a - 1]);
        }

        nodes[0].SetParent();
        nodes[0].Solve();

        Console.WriteLine(nodes[0].Memo[3]);
    }
}
