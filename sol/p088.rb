require 'set'

def solve(n, a, bad)
    dp = [[]]

    for i in 0...n
        (dp.size - 1).downto(0) do |j|
            if dp[j].nil? || dp[j].any? { |x| bad[i].include?(x) } then
                next
            end
            if dp[j + a[i]] then
                return [dp[j + a[i]], dp[j] + [i]]
            else
                dp[j + a[i]] = dp[j] + [i]
            end
        end
    end
end

def main
    n, q = readline.split.map(&:to_i)
    a = readline.split.map(&:to_i)

    bad = Array.new(n) { Set.new }

    q.times do
        x, y = readline.split.map(&:to_i)
        bad[x - 1].add(y - 1)
        bad[y - 1].add(x - 1)
    end

    b, c = solve(n, a, bad)

    puts b.size, b.map { |bi| bi + 1 }.join(' ')
    puts c.size, c.map { |ci| ci + 1 }.join(' ')
end

main if __FILE__ == $0
