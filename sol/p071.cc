#include <algorithm>
#include <cstdio>
#include <deque>
#include <vector>

using namespace std;

class Solver
{
public:
    explicit Solver(const int n, const int k)
        : nodes_(n + 1), k_(k)
    {
    }

    void add_constraint(const int a, const int b)
    {
        nodes_[a].adj.push_back(b);
        ++nodes_[b].cnt;
    }

    const vector<vector<int>>& solve()
    {
        for (int v = 1; v < nodes_.size(); v++) {
            if (nodes_[v].cnt == 0)
                add_constraint(0, v);
        }

        vis_.assign(nodes_.size(), 0);
        seq_.reserve(nodes_.size());
        if (detect_loop(0))
            return ans_;
        if (count(vis_.begin(), vis_.end(), 0) != 0)
            return ans_;
        search_ans(0);
        if (ans_.size() < k_) ans_.clear();
        return ans_;
    }

private:
    struct Node
    {
        vector<int> adj;
        int cnt;
    };

    bool detect_loop(const int u)
    {
        if (vis_[u] != 0)
            return vis_[u] == 1;
        vis_[u] = 1;
        for (const int v : nodes_[u].adj)
            if (detect_loop(v))
                return true;
        vis_[u] = 2;
        return false;
    }

    void search_ans(const int u)
    {
        seq_.push_back(u);
        if (seq_.size() == nodes_.size()) {
            ans_.push_back(seq_);
            seq_.pop_back();
            return;
        }
        for (const int v : nodes_[u].adj) {
            if (--nodes_[v].cnt == 0)
                cue_.push_back(v);
        }
        const int n = cue_.size();
        for (int i = 0; i < n; i++) {
            swap(cue_[0], cue_[i]);

            const int v = cue_[0];
            cue_.pop_front();
            search_ans(v);
            if (ans_.size() >= k_) return;
            cue_.push_front(v);
        }
        if (!cue_.empty()) {
            rotate(cue_.begin(), cue_.begin() + 1, cue_.end());
        }
        for (const int v : nodes_[u].adj) {
            if (++nodes_[v].cnt == 1)
                cue_.pop_back();
        }
        seq_.pop_back();
    }

    vector<Node> nodes_;
    const int k_;
    vector<int> vis_;
    vector<int> seq_;
    deque<int> cue_;
    vector<vector<int>> ans_;
};

int main()
{
    int n, m, k;
    scanf("%d%d%d", &n, &m, &k);
    Solver solver(n, k);

    for (int i = 0; i < m; i++) {
        int a, b; scanf("%d%d", &a, &b);
        solver.add_constraint(a, b);
    }

    const vector<vector<int>> ans = solver.solve();
    if (ans.empty()) {
        puts("-1");
    } else {
        for (const vector<int>& seq : ans) {
            for (int i = 1; i < seq.size(); i++) {
                if (i > 1) putchar(' ');
                printf("%d", seq[i]);
            }
            putchar('\n');
        }
    }

    return 0;
}
