require 'set'

n = readline.to_i

users = Set.new
for day in 1..n
    user = readline
    puts day unless users.include?(user)
    users.add(user)
end
