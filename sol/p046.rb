N = 46

readline

a, b, c = readlines.map! do |line|
    array = [0] * N
    line.chomp!.split.each { |s| array[s.to_i % N] += 1 }
    array
end

s = 0

for ak in 0...N
for bk in 0...N
    ck = (2 * N - ak - bk) % N
    s += a[ak] * b[bk] * c[ck]
end; end

puts s
