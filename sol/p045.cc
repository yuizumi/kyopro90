#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, k; cin >> n >> k;
    vector<long> x(n), y(n);
    for (int i = 0; i < n; i++)
        cin >> x[i] >> y[i];

    vector<vector<long>> dp(k + 1, vector<long>(1 << n));

    for (int m = 1; m < (1 << n); m++) {
        for (int i = 0; i < n; i++)
        for (int j = 0; j < i; j++) {
            if ((m & (1 << i)) != 0 && (m & (1 << j)) != 0) {
                const long dx = x[i] - x[j];
                const long dy = y[i] - y[j];
                dp[1][m] = max(dp[1][m], dx * dx + dy * dy);
            }
        }
    }

    vector<int> mask0, mask1;
    mask0.reserve(14283372);
    mask1.reserve(14283372);

    for (int i = 1; i < n; i++) {
        for (int m0 = (1 << i); m0 < (2 << i); m0++) {
            for (int m1 = (1 << 0); m1 < (1 << i); m1++) {
                if ((m0 & m1) == 0) {
                    mask0.push_back(m0);
                    mask1.push_back(m1);
                }
            }
        }
    }

    for (int j = 2; j <= k; j++) {
        dp[j] = dp[j-1];
        for (int p = 0; p < mask0.size(); p++) {
            const int m0 = mask0[p];
            const int m1 = mask1[p];
            dp[j][m0|m1] = min(dp[j][m0|m1], max(dp[j-1][m0], dp[1][m1]));
        }
    }

    cout << dp[k][(1 << n) - 1] << endl;

    return 0;
}
