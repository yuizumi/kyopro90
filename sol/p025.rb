require 'set'

f_set = Set.new
f_set.add(0)

for a0 in  1..9
for a1 in a0..9
for a2 in a1..9
for a3 in a2..9
for a4 in a3..9
for a5 in a4..9
for a6 in a5..9
for a7 in a6..9
for a8 in a7..9
for a9 in a8..9
    f_set.add(a0 * a1 * a2 * a3 * a4 * a5 * a6 * a7 * a8 * a9)
end; end; end; end; end; end; end; end; end; end

n, b = readline.split.map(&:to_i)

puts (
    f_set.count do |f_m|
        m = b + f_m
        prod = 1
        k = m
        while k > 0
            prod *= k % 10; k /= 10
        end
        prod == f_m && m <= n
    end
)
