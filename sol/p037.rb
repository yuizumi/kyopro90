class OnlineMax
    def initialize(size)
        @n = 2 ** Math.log2(size).ceil
        @a = [-1] * (2 * @n)
    end

    def update(i, value)
        k = @n + i
        while k > 0
            @a[k] = value if @a[k] < value
            k /= 2
        end
    end

    def query(min, max)
        do_query(min, max, 1, 0, @n)
    end

    def do_query(min, max, i, min_i, max_i)
        if max <= min_i || max_i <= min then
            return -1
        end
        if min <= min_i && max_i <= max then
            return @a[i]
        end
        mid_i = (min_i + max_i) / 2
        lo = do_query(min, max, 2 * i + 0, min_i, mid_i)
        hi = do_query(min, max, 2 * i + 1, mid_i, max_i)
        [lo, hi].max
    end
end

def main
    w, n = readline.split.map(&:to_i)

    om = OnlineMax.new(w + 1)
    om.update(0, 0)

    n.times do
        l, r, v = readline.split.map(&:to_i)
        w.downto(r) do |i|
            q = om.query(i - r, i - l + 1)
            om.update(i, q + v) if q >= 0
        end
        r.downto(l) do |i|
            q = om.query(0, i - l + 1)
            om.update(i, q + v) if q >= 0
        end
    end

    puts om.query(w, w + 1)
end

main if __FILE__ == $0
