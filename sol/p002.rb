def solve(s, n, depth)
    if n == 0 then
        puts s
    else
        solve(s + '(', n - 1, depth + 1) if depth < n
        solve(s + ')', n - 1, depth - 1) if depth > 0
    end
end

n = readline.to_i
solve('', n , 0) if n % 2 == 0
