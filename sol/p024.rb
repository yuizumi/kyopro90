n, k = readline.split.map(&:to_i)
a = readline.split.map(&:to_i)
b = readline.split.map(&:to_i)

d = a.zip(b).map { |ai, bi| (ai - bi).abs }.inject(0, &:+)

if d <= k && d % 2 == k % 2 then
    puts 'Yes'
else
    puts 'No'
end
