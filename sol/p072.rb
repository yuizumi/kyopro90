require 'thread'

State = Struct.new(:vis, :pos, :src)

h, w = readline.split.map(&:to_i)
w += 1
grid = readlines.join + ('#' * w)

queue = Queue.new
dp = {}
for pos in 0...(h * w)
    if grid[pos] == '.' then
        q = State.new(1 << pos, pos, pos)
        queue.push(q)
        dp[q] = 1
    end
end

ans = 2

until queue.empty?
    q0 = queue.pop

    for δ in [-w, -1, +1, +w]
        pos = q0.pos + δ

        next if grid[pos] != '.'
        q1 = State.new(q0.vis | (1 << pos), pos, q0.src)
        next if dp.include?(q1)
        if q1.vis != q0.vis then
            dp[q1] = dp[q0] + 1
            queue.push(q1)
        elsif q1.pos == q0.src then
            ans = [ans, dp[q0]].max
        end
    end
end

puts (ans == 2) ? -1 : ans
