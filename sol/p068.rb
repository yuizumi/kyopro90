Query = Struct.new(:t, :x, :y, :v)
Expr = Struct.new(:a, :b)

class Node
    def initialize
        @pa = self
    end

    def root
        (@pa == self) ? @pa : (@pa = @pa.root)
    end

    attr_accessor :pa
end

def main
    n = readline.to_i
    q = readline.to_i

    queries = Array.new(q) do
        t, x, y, v = readline.split.map(&:to_i)
        Query.new(t, x - 1, y - 1, v)
    end

    v = Array.new(n)

    for q in queries
        v[q.y] = q.v if q.t == 0
    end

    exprs = Array.new(n)

    for i in 0...n
        if v[i].nil? then
            exprs[i] = Expr.new(1, 0)
        else
            e = exprs[i - 1]
            exprs[i] = Expr.new(-e.a, v[i] - e.b)
        end
    end

    uf = Array.new(n) { Node.new }

    for q in queries
        rx = uf[q.x].root
        ry = uf[q.y].root

        if q.t == 0 then
            rx.pa = ry if rx != ry
            next
        end

        if rx == ry then
            ex = exprs[q.x]
            ey = exprs[q.y]
            puts ey.a * (q.v - ex.b) / ex.a + ey.b
        else
            puts 'Ambiguous'
        end
    end
end

main if __FILE__ == $0
