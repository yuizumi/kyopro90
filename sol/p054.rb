require 'thread'

n, m = readline.split.map!(&:to_i)

cv = Array.new(n) { [] }
papers = []
m.times do
    k = readline.to_i
    r = readline.split.map! do |s|
        s.to_i - 1
    end
    papers.push(r)
    for i in 0...k
        cv[r[i]].push(papers.size - 1)
    end
end

queue = Queue.new
queue.enq(0)

tak = [-1] * n
tak[0] = 0
until queue.empty?
    a = queue.deq
    for pi in cv[a]
        next if !papers[pi]
        for ri in papers[pi]
            next if tak[ri] >= 0
            queue.enq(ri)
            tak[ri] = tak[a] + 1
        end
        papers[pi] = nil
    end
end

puts tak
