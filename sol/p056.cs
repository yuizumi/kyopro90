using System;
using System.Linq;

static class P056
{
    static void Main()
    {
        string[] ns = Console.ReadLine().Split();
        int n = Int32.Parse(ns[0]);
        int s = Int32.Parse(ns[1]);

        var dp = new char[n + 1, s + 1];
        dp[0, 0] = '*';

        var a = new int[n];
        var b = new int[n];
        for (int i = 0; i < n; i++)
        {
            string[] ab = Console.ReadLine().Split();
            a[i] = Int32.Parse(ab[0]);
            b[i] = Int32.Parse(ab[1]);

            for (int j = 0; j <= s; j++)
            {
                if (j >= a[i] && dp[i, j - a[i]] != '\0') dp[i + 1, j] = 'A';
                if (j >= b[i] && dp[i, j - b[i]] != '\0') dp[i + 1, j] = 'B';
            }
        }

        if (dp[n, s] != '\0')
        {
            var ans = new char[n];

            for (int i = n - 1; i >= 0; i--)
            {
                ans[i] = dp[i + 1, s];
                s -= (dp[i + 1, s] == 'A') ? a[i] : b[i];
            }
            Console.WriteLine(new string(ans));
        }
        else
        {
            Console.WriteLine("Impossible");
        }
    }
}
