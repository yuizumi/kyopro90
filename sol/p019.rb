n = 2 * readline.to_i
a = readline.split.map(&:to_i)

dp = [[0] * (2 * n)]
for k in (2..n).step(2)
    dp[k] = []
    for j in 0..(n - k)
        dp[k][j] = (a[j] - a[j+k-1]).abs + dp[k-2][j+1]
        for i in (2..(k-2)).step(2)
            dp[k][j] = [dp[k][j], dp[i][j] + dp[k-i][j+i]].min
        end
    end
end

puts dp[n][0]
