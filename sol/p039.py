import sys

sys.setrecursionlimit(1000000)


class Metrics(object):
    def __init__(self, dist, size):
        self.dist = dist
        self.size = size


class Node(object):
    def __init__(self):
        self.subnodes = []
        self.parent = None
        self.inner = Metrics(0, 1)
        self.outer = Metrics(0, 1)


def traverse(u):
    for v in u.subnodes:
        v.subnodes.remove(u)
        v.parent = u
        traverse(v)


def compute_inner(u):
    for v in u.subnodes:
        compute_inner(v)
        u.inner.dist += v.inner.dist + v.inner.size
        u.inner.size += v.inner.size


def compute_outer(v):
    u = v.parent
    if u:
        v.outer.dist += u.inner.dist + u.outer.dist
        v.outer.dist -= v.inner.dist + v.inner.size
        v.outer.size += u.inner.size + u.outer.size
        v.outer.size -= v.inner.size + 1
        v.outer.dist += v.outer.size - 1
    for w in v.subnodes: compute_outer(w)


def main():
    n = int(input())

    nodes = [Node() for _ in range(n)]
    for _ in range(n - 1):
        a, b = (int(s) - 1 for s in input().split())
        nodes[a].subnodes.append(nodes[b])
        nodes[b].subnodes.append(nodes[a])

    root = nodes[0]
    traverse(root)
    compute_inner(root)
    compute_outer(root)

    ans = sum(v.inner.dist + v.outer.dist for v in nodes)
    print(ans // 2)


if __name__ == '__main__':
    main()
