require 'thread'

n = readline.to_i

a = Array.new(n)
b = Array.new(n)
g = Array.new(n) { [] }

for i in 0...n
    a[i], b[i] = readline.split.map(&:to_i)
    a[i] -= 1; g[a[i]].push(i)
    b[i] -= 1; g[b[i]].push(i)
end

y = [-1] * n
q = Queue.new
m = n

for i in 0...n
    if a[i] == i || b[i] == i then
        q.enq(i)
        y[i] = (m -= 1)
    end
end

until q.empty? || m == 0
    i = q.deq
    for j in g[i]
        if y[j] == -1 then
            q.enq(j)
            y[j] = (m -= 1)
        end
    end
end

if m == 0 then
    x = Array.new(n)
    (0...n).each { |i| x[y[i]] = i + 1 }
    puts x
else
    puts (-1)
end
