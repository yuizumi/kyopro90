n, l = readline.split.map(&:to_i)
k = readline.to_i
a = readline.split.map(&:to_i) + [l]

lo = 0
hi = l
while lo + 1 < hi
    m = (lo + hi) / 2
    done = 0
    cnt = 0
    for a_i in a
        if a_i - done >= m then
            done = a_i
            cnt += 1
        end
    end
    if cnt >= k + 1 then
        lo = m
    else
        hi = m
    end
end

puts lo
