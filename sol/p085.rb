n = readline.to_i
ans = 0

p = 1
while (p * p * p) <= n
    if n % p == 0 then
        m = n / p
        q = p
        while (q * q) <= m
            ans += 1 if m % q == 0
            q += 1
        end
    end
    p += 1
end

puts ans
