M = 10 ** 9 + 7

lo, hi = readline.split.map(&:to_i)
ans = 0

for k in 1..19
    if lo >= 10 ** k then
        next
    end
    if hi <  10 ** k then
        ans = (ans + (hi + lo) * (hi - lo + 1) / 2 * k) % M
        break
    else
        md = 10 ** k - 1
        ans = (ans + (md + lo) * (md - lo + 1) / 2 * k) % M
        lo = md + 1
    end
end

puts ans
