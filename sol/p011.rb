Task = Struct.new(:d, :c, :s)

n = readline.to_i
tasks = Array.new(n) do
    Task.new(*readline.split.map(&:to_i))
end

tasks.sort_by! { |t| -t.d }
profit = [0] * 5001

for t in tasks
    d, c, s = t.d, t.c, t.s
    for k in 0..(d - c)
        profit[k] = [profit[k], profit[k + c] + s].max
    end
end

puts profit[0]
