using System;
using System.Collections.Generic;

class Node
{
    public int Order { get; private set; } = -1;
    public int Depth { get; private set; } = -1;
    public List<Node> Edges { get; } = new List<Node>();
    public List<Node> Ancestors { get; } = new List<Node>();
    public Node Final { get; private set; }

    public Node Traverse(int order, int depth)
    {
        Order = order;
        Depth = depth;
        Final = this;
        foreach (Node that in Edges)
        {
            if (that.Order != -1) continue;
            that.Ancestors.Add(this);
            Final = that.Traverse(Final.Order + 1, Depth + 1);
        }
        return Final;
    }

    public static Node CommonAncestor(Node u, Node v)
    {
        while (u.Final.Order < v.Order)
        {
            for (int i = u.Ancestors.Count - 1; i >= 1; i--)
            {
                if (u.Ancestors[i].Final.Order < v.Order)
                {
                    u = u.Ancestors[i];
                    break;
                }
            }
            u = u.Ancestors[0];
        }

        return u;
    }
}

static class P035
{
    static void Main()
    {
        int n = Int32.Parse(Console.ReadLine());
        var nodes = new Node[n];
        for (int i = 0; i < n; i++)
            nodes[i] = new Node();
        for (int i = 0; i < n - 1; i++)
        {
            string[] uv = Console.ReadLine().Split();
            int u = Int32.Parse(uv[0]) - 1;
            int v = Int32.Parse(uv[1]) - 1;
            nodes[u].Edges.Add(nodes[v]);
            nodes[v].Edges.Add(nodes[u]);
        }

        nodes[0].Traverse(0, 0);

        for (int i = 0; ; i++) {
            bool updated = false;
            foreach (Node v in nodes)
            {
                Node u;
                if (i >= v.Ancestors.Count) continue;
                u = v.Ancestors[i];
                if (i >= u.Ancestors.Count) continue;
                u = u.Ancestors[i];
                v.Ancestors.Add(u);
                updated = true;
            }
            if (!updated) break;
        }

        int q = Int32.Parse(Console.ReadLine());
        while (q-- > 0)
        {
            string[] row = Console.ReadLine().Split();
            int k = Int32.Parse(row[0]);
            var v = new Node[k];
            for (int i = 0; i < k; i++)
                v[i] = nodes[Int32.Parse(row[i + 1]) - 1];
            Array.Sort(v, (x, y) => x.Order - y.Order);

            Node root = v[0];
            int ans = 0;
            for (int i = 1; i < k; i++)
            {
                Node u = Node.CommonAncestor(v[i - 1], v[i]);
                if (root.Depth > u.Depth)
                {
                    ans += root.Depth - u.Depth;
                    root = u;
                }
                ans += v[i].Depth - u.Depth;
            }

            Console.WriteLine(ans);
        }
    }
}
