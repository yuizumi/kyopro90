Edge = Struct.new(:c, :u, :v)

class DisjointSet
    class Node
        def initialize
            @pa = nil
            @rank = 0
        end

        def parent=(node)
            @pa = node
            node.rank += 1
        end

        def root
            @pa ? (@pa = @pa.root) : self
        end

        attr_accessor :rank
    end

    def initialize(n)
        @nodes = Array.new(n) { Node.new }
    end

    def union(i, j)
        u = @nodes[i].root
        v = @nodes[j].root
        if u == v then
            return false
        else
            u, v = v, u if u.rank < v.rank
            v.parent = u
            return true
        end
    end
end

def main
    n, m = readline.split.map!(&:to_i)

    edges = readlines.map! do |line|
        c, l, r = line.split.map!(&:to_i)
        Edge.new(c, l - 1, r)
    end
    edges.sort_by! { |e| e.c }

    ds = DisjointSet.new(n + 1)
    cost = 0
    rest = n
    for e in edges
        next unless ds.union(e.u, e.v)
        cost += e.c
        rest -= 1
        break if rest == 0
    end

    puts (rest == 0) ? cost : -1
end

main if __FILE__ == $0
