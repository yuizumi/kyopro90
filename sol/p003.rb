def solve(graph, curr, prev)
    max_depth = 0
    max_score = 0

    for node in graph[curr]
        next if node == prev

        depth, score = solve(graph, node, curr)
        max_score = [max_score, score, depth + max_depth].max
        max_depth = [max_depth, depth].max
    end

    [max_depth + 1, max_score]
end

n = readline.to_i

graph = Array.new(n + 1) { [] }
(n - 1).times do
    a_i, b_i = readline.split.map(&:to_i)
    graph[a_i].push(b_i)
    graph[b_i].push(a_i)
end

puts solve(graph, 1, nil)[1] + 1
