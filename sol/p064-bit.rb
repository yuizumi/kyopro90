class OnlineSum
    def initialize(n)
        @n = 2 ** Math.log2(n).ceil
        @data = [0] * (@n + 1)
    end

    def add(i, val)
        while i <= @n
            @data[i] += val
            i += i & -i
        end
    end

    def sum(i)
        acc = 0
        while i > 0
            acc += @data[i]
            i -= i & -i
        end
        acc
    end
end

def main
    n, q = readline.split.map!(&:to_i)

    a = ['0']
    a.concat(readline.split)
    a.map!(&:to_i)

    ans = (2..n).inject(0) do |sum, i|
        sum + (a[i-1] - a[i]).abs
    end

    os = OnlineSum.new(n)
    q.times do
        l, r, v = readline.split.map(&:to_i)

        if l > 1 then
            lo = a[l - 1] + os.sum(l - 1)
            hi = a[l - 0] + os.sum(l - 0)
            ans -= (lo - hi).abs
        end
        if r < n then
            lo = a[r + 0] + os.sum(r + 0)
            hi = a[r + 1] + os.sum(r + 1)
            ans -= (lo - hi).abs
        end

        os.add(l, v)
        os.add(r + 1, -v) if r < n

        if l > 1 then
            lo = a[l - 1] + os.sum(l - 1)
            hi = a[l - 0] + os.sum(l - 0)
            ans += (lo - hi).abs
        end
        if r < n then
            lo = a[r + 0] + os.sum(r + 0)
            hi = a[r + 1] + os.sum(r + 1)
            ans += (lo - hi).abs
        end

        puts ans
    end
end

main if __FILE__ == $0
