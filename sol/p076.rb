def solve(n, a)
    m = a.inject(0, &:+)

    return false if m % 10 != 0

    m /= 10
    b = a + a
    i = j = 0
    val = 0
    while i < n
        return true if val == m

        if val < m then
            val += b[j]; j += 1
        else
            val -= b[i]; i += 1
        end
    end

    return false
end

def main
    n = readline.to_i
    a = readline.split.map(&:to_i)
    ans = solve(n, a)
    puts(ans ? 'Yes' : 'No')
end

main if __FILE__ == $0
