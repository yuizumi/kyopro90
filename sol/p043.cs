using System;
using System.Collections.Generic;

static class P043
{
    private static readonly int[] Dx = {1, 0, -1, 0};
    private static readonly int[] Dy = {0, 1, 0, -1};

    static void Main()
    {
        string[] hw = Console.ReadLine().Split();
        int h = Int32.Parse(hw[0]);
        int w = Int32.Parse(hw[1]);

        string[] sysx = Console.ReadLine().Split();
        int sy = Int32.Parse(sysx[0]);
        int sx = Int32.Parse(sysx[1]);
        string[] tytx = Console.ReadLine().Split();
        int ty = Int32.Parse(tytx[0]);
        int tx = Int32.Parse(tytx[1]);

        var grid = new char[h + 2, w + 2];
        for (int y = 1; y <= h; y++)
        {
            string line = Console.ReadLine();
            for (int x = 1; x <= w; x++)
                grid[y, x] = line[x - 1];
        }

        var ans = new int[h + 2, w + 2, 4];
        ans[sy, sx, 0] = 1;
        ans[sy, sx, 1] = 1;
        ans[sy, sx, 2] = 1;
        ans[sy, sx, 3] = 1;

        var queue = new Queue<(int, int, int)>();
        queue.Enqueue((sy, sx, 1));

        while (true)
        {
            (int y0, int x0, int n) = queue.Dequeue();

            for (int d = 0; d < 4; d++)
            {
                int y = y0 + Dy[d], x = x0 + Dx[d];

                while (grid[y, x] == '.' && ans[y, x, d] == 0)
                {
                    if (y == ty && x == tx)
                    {
                        Console.WriteLine(n - 1);
                        return;
                    }

                    queue.Enqueue((y, x, n + 1));
                    ans[y, x, d] = n;
                    y += Dy[d]; x += Dx[d];
                }
            }
        }
    }
}
