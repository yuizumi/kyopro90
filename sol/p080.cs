using System;
using System.Linq;

static class P080
{
    static void Main()
    {
        string[] nd = Console.ReadLine().Split();

        int n = Int32.Parse(nd[0]);
        int d = Int32.Parse(nd[1]);
        long[] a = Console.ReadLine().Split().Select(Int64.Parse).ToArray();

        long[,] dp = new long[d + 1, 1 << n];
        dp[0, 0] = 1;

        for (int i = 0; i < d; i++)
        {
            for (int j = 0; j < (1 << n); j++)
                dp[i + 1, j] = dp[i, j];

            int m = 0;
            for (int j = 0; j < n; j++)
                if ((a[j] & (1L << i)) != 0) m |= 1 << j;

            for (int j = 0; j < (1 << n); j++)
                dp[i + 1, j | m] += dp[i, j];
        }

        Console.WriteLine(dp[d, (1 << n) - 1]);
    }
}
