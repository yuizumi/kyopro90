#include <algorithm>
#include <bitset>
#include <cstdio>
#include <unordered_map>
#include <vector>

using namespace std;

using State = bitset<100>;

class Solver
{
public:
    explicit Solver(const int n)
        : gain_(n),
          deps_(n),
          memo_(n)
    {
        for (int i = 0; i < n; i++) deps_[i].set(i);
    }

    void set_gain(int room, int gain)
    {
        gain_[room] = gain;
    }

    void add_key(int room, int key)
    {
        deps_[key] |= deps_[room];
    }

    long max_gain(const vector<int>& rooms, int k, const State& deps)
    {
        if (k == rooms.size()) {
            return 0;
        }

        const int room = rooms[k];

        const auto [it, inserted] = memo_[room].try_emplace(deps, LONG_MIN);
        long& value = it->second;
        if (!inserted) {
            return value;
        }
        if (!deps[room]) value = max(value, max_gain(rooms, k + 1, deps));
        const State new_deps = (deps | deps_[room]).reset(room);
        value = max(value, max_gain(rooms, k + 1, new_deps) + gain_[room]);
        return value;
    }

private:
    vector<long> gain_;
    vector<State> deps_;
    vector<unordered_map<State, long>> memo_;
};

int main()
{
    int n, w; scanf("%d%d", &n, &w);
    Solver solver(n);

    for (int i = 0; i < n; i++) {
        int a; scanf("%d", &a);
        solver.set_gain(i, a - w);
    }

    vector<int> group(n);

    for (int i = 0; i < n; i++) {
        group[i] = i;
    }
    for (int i = 0; i < n; i++) {
        int k; scanf("%d", &k);
        for (int j = 0; j < k; j++) {
            int c; scanf("%d", &c);
            solver.add_key(i, c - 1);

            const int g_old = min(group[i], group[c - 1]);
            const int g_new = max(group[i], group[c - 1]);
            if (g_old != g_new) {
                replace(group.begin(), group.end(), g_old, g_new);
            }
        }
    }

    vector<int> rooms;
    rooms.reserve(n);

    long ans = 0;
    for (int i = n - 1; i >= 0; i--) {
        if (group[i] != i) continue;
        rooms.clear();
        for (int j = i; j >= 0; j--) {
            if (group[j] == i) rooms.push_back(j);
        }
        ans += solver.max_gain(rooms, 0, State());
    }
    printf("%ld\n", ans);
    return 0;
}
