n = readline.to_i
a = Array.new(n) do
    readline.split.map(&:to_i)
end

b = Array.new(n) { [] }
m = readline.to_i
m.times do
    x, y = readline.split.map { |s| s.to_i - 1 }
    b[x][y] = b[y][x] = true
end

dp0 = Array.new(1 << n) { [] }

for i in 0...n
    dp0[1 << i][i] = a[i][0]
end

for k in 1...n
    dp1 = Array.new(1 << n) { [] }

    for q0 in 0...(1 << n)
        for i in 0...n
            q1 = q0 | (1 << i)
            next if q1 == q0
            for j in 0...n
                if b[i][j] || !dp0[q0][j] then
                    next
                end
                value = dp0[q0][j] + a[i][k]
                dp1[q1][i] = value if !dp1[q1][i] || (dp1[q1][i] > value)
            end
        end
    end

    dp0 = dp1
end

puts dp0[(1 << n) - 1].min
