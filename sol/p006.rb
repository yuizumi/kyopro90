n, k = readline.split.map(&:to_i)
s = readline.chomp

table = {}

for c in 'abcdefghijklmnopqrstuvwxyz'.each_char
    table[c] = []
end

for c, i in s.each_char.each_with_index do
    table[c].push(i)
end

for c in 'abcdefghijklmnopqrstuvwxyz'.each_char
    table[c].push(n)  # sentinel
    table[c].reverse!
end

ans = ''
j = 0
while ans.length < k
    for c in 'abcdefghijklmnopqrstuvwxyz'.each_char
        while table[c][-1] < j
            table[c].pop
        end
        if table[c][-1] <= n - (k - ans.length)
            j = table[c][-1] + 1
            ans += c
            break
        end
    end
end

puts ans
