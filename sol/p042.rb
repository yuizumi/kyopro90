M = 10**9 + 7

k = readline.to_i

if k % 9 != 0 then
    puts 0
else
    dp = [1]
    for i in 1..k
        dp[i] = 0
        for j in (i-9)..(i-1)
            dp[i] = (dp[i] + dp[j]) % M if j >= 0
        end
    end
    puts dp[k]
end
