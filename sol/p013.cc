#include <iostream>
#include <map>
#include <vector>

using namespace std;

constexpr long kInf = 1L << 60;

struct Edge { int u, v; long c; };

using Graph = vector<vector<Edge>>;

vector<long> dijkstra(const Graph& graph, int s)
{
    vector<long> costs(graph.size(), kInf);

    multimap<long, int> mm;
    mm.emplace(0, s);
    while (!mm.empty()) {
        const auto [cost, u] = *mm.begin();
        mm.erase(mm.begin());
        if (costs[u] < cost) continue;
        costs[u] = cost;
        for (const Edge& e : graph[u]) {
            if (costs[e.v] > cost + e.c) mm.emplace(cost + e.c, e.v);
        }
    }

    return costs;
}

int main()
{
    int n, m; cin >> n >> m;

    Graph graph(n + 1);

    for (int i = 0; i < m; i++) {
        int a, b; long c; cin >> a >> b >> c;
        graph[a].push_back({a, b, c});
        graph[b].push_back({b, a, c});
    }

    const vector<long> costs_1 = dijkstra(graph, 1);
    const vector<long> costs_n = dijkstra(graph, n);

    for (int k = 1; k <= n; k++) {
        cout << costs_1[k] + costs_n[k] << endl;
    }

    return 0;
}
