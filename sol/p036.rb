n, q = readline.split.map(&:to_i)

add = []
sub = []

n.times do
    x, y = readline.split.map(&:to_i)
    add.push(x + y)
    sub.push(x - y)
end

add_max = add.max
add_min = add.min
sub_max = sub.max
sub_min = sub.min

q.times do
    i = readline.to_i - 1
    puts [
        add_max - add[i],
        add[i] - add_min,
        sub_max - sub[i],
        sub[i] - sub_min,
    ].max
end
