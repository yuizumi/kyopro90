n, p, q = STDIN.gets.not_nil!.split.map { |s| s.to_i64 }
a = STDIN.gets.not_nil!.split.map { |s| s.to_i64 }

ans = 0
k0 = n
(0...k0).each { |k1| a1 = a[k1]
(0...k1).each { |k2| a2 = a[k2]
(0...k2).each { |k3| a3 = a[k3]
(0...k3).each { |k4| a4 = a[k4]
(0...k4).each { |k5| a5 = a[k5]
    ans += 1 if (a1 * a2 % p * a3 % p * a4 % p * a5 % p) == q
}}}}}

puts ans
