#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

vector<int> build_suffix_array(const string& s)
{
    const int n = s.length();

    vector<int> index(n);
    vector<int> rank0(n + n, -1);
    vector<int> rank1(n + n, -1);

    for (int i = 0; i < n; i++) {
        index[i] = i;
    }
    sort(index.begin(), index.end(), [&](int x, int y) {
        return s[x] < s[y];
    });
    rank0[index[n - 1]] = n - 1;
    for (int i = n - 2; i >= 0; i--) {
        const int x = index[i];
        const int y = index[i + 1];
        rank0[x] = (s[x] == s[y]) ? rank0[y] : i;
    }

    for (int k = 1; k < n; k *= 2) {
        for (int i = 0; i < n; ) {
            const int j = rank0[index[i]] + 1;
            sort(index.begin() + i, index.begin() + j, [&](int x, int y) {
                return rank0[x + k] < rank0[y + k];
            });
            i = j;
        }

        rank1[index[n - 1]] = n - 1;

        for (int i = n - 2; i >= 0; i--) {
            const int x = index[i];
            const int y = index[i + 1];
            rank1[x] = (rank0[x] == rank0[y] && rank0[x + k] == rank0[y + k])
                ? rank1[y] : i;
        }

        swap(rank0, rank1);
    }

    return index;
}

int solve(const vector<int>& sa, const string& s, const string& t)
{
    int n = 0, k = 0;
    for (const int index : sa) {
        if (k > s.length() - index) break;
        if (k > 0 && t[k - 1] != s[index + k - 1]) break;
        while (k < s.length() - index && t[k] == s[index + k]) ++k;
        if (k == s.length() - index) ++n;
    }
    return n;
}

string swap_rgb(string s, char x, char y)
{
    for (auto p = s.begin(); p != s.end(); ++p) {
        if (*p == x)
            *p = y;
        else if (*p == y)
            *p = x;
    }
    return s;
}

int main()
{
    int n; cin >> n;
    string s; cin >> s;
    string t; cin >> t;

    int ans = 0;
    if (s.length() >= 1) {
        const vector<int> sa = build_suffix_array(s);
        ans += solve(sa, s, swap_rgb(t, 'B', 'G'));
        ans += solve(sa, s, swap_rgb(t, 'R', 'B'));
        ans += solve(sa, s, swap_rgb(t, 'G', 'R'));
    }
    if (t.length() >= 2) {
        t = t.substr(1);
        const vector<int> sa = build_suffix_array(t);
        ans += solve(sa, t, swap_rgb(s, 'B', 'G'));
        ans += solve(sa, t, swap_rgb(s, 'R', 'B'));
        ans += solve(sa, t, swap_rgb(s, 'G', 'R'));
    }
    cout << ans << endl;
    return 0;
}
