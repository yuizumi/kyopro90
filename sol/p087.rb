def calc_price(n, p, k, a)
    lo, hi = 1, p + 2

    while lo < hi
        x = (lo + hi) / 2

        d = a.map do |a_i|
            a_i.map { |a_ij| (a_ij == -1) ? x : a_ij }
        end

        for h in 0...n
        for i in 0...n
        for j in 0...n
            d[i][j] = [d[i][j], d[i][h] + d[h][j]].min
        end; end; end

        m = 0

        for i in 0...n
        for j in 0...i
            m += 1 if d[i][j] <= p
        end; end

        (m > k) ? (lo = x + 1) : (hi = x)
    end

    lo
end

def main
    n, p, k = readline.split.map(&:to_i)

    a = Array.new(n) do
        readline.split.map(&:to_i)
    end

    lo = calc_price(n, p, k, a)
    hi = calc_price(n, p, k - 1, a)
    infinity = p + 2

    if lo < infinity && hi == infinity then
        puts 'Infinity'
    else
        puts hi - lo
    end
end

main if __FILE__ == $0
