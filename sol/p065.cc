#include <algorithm>
#include <iostream>
#include <vector>

#include <atcoder/convolution>
#include <atcoder/modint>

using namespace atcoder;
using namespace std;

using mint = modint998244353;

ostream& operator<<(ostream& os, mint x)
{
    return os << x.val();
}

vector<mint> combo(int n, int min)
{
    vector<mint> out(n + 1);
    mint c = 1;
    for (int i = 0; i <= n; i++) {
        if (i >= min)
            out[i] = c;
        c = c * (n - i) / (i + 1);
    }
    return out;
}

int main()
{
    int r, g, b, k;
    cin >> r >> g >> b >> k;
    int x, y, z;
    cin >> x >> y >> z;

    const vector<mint> cr = combo(r, max(k - y, 0));
    const vector<mint> cg = combo(g, max(k - z, 0));
    const vector<mint> cb = combo(b, max(k - x, 0));

    const vector<mint> ans = convolution(cr, convolution(cg, cb));    
    cout << ans[k].val() << endl;

    return 0;
}
