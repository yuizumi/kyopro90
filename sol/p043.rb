INF = 999999999

h, w = readline.split.map(&:to_i)
h += 1; w += 1

sy, sx = readline.split.map(&:to_i)
s = sy * w + sx - 1
ty, tx = readline.split.map(&:to_i)
t = ty * w + tx - 1

grid = ('#' * w) + readlines.join('') + ('#' * w)
memo = Array.new(h * w) { [INF] * 4 }

Δ = [1, w, -1, -w]

queue = [s]
qi = -1
memo[s] = [-1] * 4

loop do
    u = queue[qi += 1]
    n = memo[u].min + 1

    for i in 0..3
        d = Δ[i]
        v = u + d
        while grid[v] == '.' && memo[v][i] == INF
            if v == t then
                puts n
                exit
            end
            queue.push(v)
            memo[v][i] = n
            v += d
        end
    end
end
