head = []
tail = []

q = readline.to_i
q.times do
    t, x = readline.split.map(&:to_i)
    case t
    when 1
        head.push(x)
    when 2
        tail.push(x)
    when 3
        if x <= head.size then
            puts head[-x]
        else
            puts tail[(x - 1) - head.size]
        end
    end
end
