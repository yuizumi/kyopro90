#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n, k; cin >> n >> k;

    vector<int> min(n + 1);
    min[1] = 1;
    vector<int> num(n + 1);
    int ans = 0;

    for (int i = 2; i <= n; i++) {
        if (min[i] != 0) {
            const int j = i / min[i];
            if (min[i] == min[j]) {
                num[i] = num[j];
            } else {
                num[i] = num[j] + 1;
            }
        } else {
            min[i] = i;
            num[i] = 1;
            if (i * i <= n) {
                for (int j = i; j <= n; j += i) {
                    if (min[j] == 0) min[j] = i;
                }
            }
        }

        if (num[i] >= k) ++ans;
    }

    cout << ans << endl;

    return 0;
}
