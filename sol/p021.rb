def traverse(graph, u, trace, visit)
    return if visit[u]

    visit[u] = true
    for v in graph[u]
        traverse(graph, v, trace, visit)
    end
    trace.push(u)
end

def main
    n, m = readline.split.map(&:to_i)

    fw = Array.new(n + 1) { [] }
    bw = Array.new(n + 1) { [] }

    m.times do
        a, b = readline.split.map(&:to_i)
        fw[a].push(b)
        bw[b].push(a)
    end

    visit = [false] * (n + 1)
    order = []
    for v in 1..n
        traverse(fw, v, order, visit)
    end
    visit = [false] * (n + 1)
    ans = 0
    for v in order.reverse!
        trace = []
        traverse(bw, v, trace, visit)
        m = trace.size
        ans += m * (m - 1) / 2
    end

    puts ans
end

main if __FILE__ == $0
