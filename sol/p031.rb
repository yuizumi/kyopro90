require 'set'

class Grandy
    def initialize(max_w)
        @memo = Array.new(max_w + 1) { [] }
    end

    def get(w, b)
        if not @memo[w][b] then
            banned = Set.new
            for k in 1..(b/2) do
                banned.add(get(w - 0, b - k))
            end
            if w > 0 then
                banned.add(get(w - 1, b + w))
            end
            q = 0
            q += 1 while banned.include?(q)
            @memo[w][b] = q
        end
        @memo[w][b]
    end
end

def main
    readline  # N
    w = readline.split.map(&:to_i)
    b = readline.split.map(&:to_i)

    grandy = Grandy.new(w.max)
    g = 0
    w.zip(b).each { |wi, bi| g ^= grandy.get(wi, bi) }
    puts (g == 0) ? 'Second' : 'First'
end

main if __FILE__ == $0
