h, w = readline.split.map!(&:to_i)

mat = Array.new(h) do
    readline.split.map!(&:to_i)
end

ans = 0

for m in 1...(1 << h)
    freq = Hash.new(0)

    for x in 0...w
        v = nil
        n = 0
        for y in 0...h
            if m[y] == 1 then
                if mat[y][x] == (v ||= mat[y][x]) then
                    n += 1
                else
                    v = -1
                end
            end
        end
        freq[v] += n if v >= 0
    end

    freq[-1] = ans
    ans = freq.values.max
end

puts ans
