M = 998244353

n, m = readline.split.map(&:to_i)

a = [0] * m
for j in 0...n
    readline  # K
    for i in readline.split.map(&:to_i)
        a[i - 1] |= 1 << j
    end
end

s = readline.split.map(&:to_i)

for i in 0...m
    a[i] |= (s[i] << n)
end

j = 0

for i in 0...n
    break if j >= m

    if a[j][i] == 0 then
        x = (j...m).find { |k| a[k][i] == 1 }
        next if x.nil?
        a[j] ^= a[x]
    end
    j += 1
    for k in j...m
        a[k] ^= a[j - 1] if a[k][i] == 1
    end
end

if (j...m).any? { |k| a[k][n] != 0 } then
    puts 0
else
    puts 2 ** (n - j) % M
end
