#include <iostream>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/number.hpp>

using namespace boost::multiprecision;
using namespace std;

constexpr long MAX = 1000000000000000000L;

int main()
{
    cpp_int a, b;
    cin >> a >> b;
    if (const cpp_int ans = lcm(a, b); ans > MAX) {
        cout << "Large" << endl;
    } else {
        cout << ans << endl;
    }
    return 0;
}
