using System;
using System.Collections.Generic;
using System.Linq;

static class P034
{
    static void Main()
    {
        int[] nk = Console.ReadLine().Split().Select(Int32.Parse).ToArray();
        int n = nk[0], k = nk[1];
        int[] a  = Console.ReadLine().Split().Select(Int32.Parse).ToArray();

        var i2a = new SortedDictionary<int, int>();
        var a2i = new SortedDictionary<int, int>();
        int done = 0;
        int ans = 0;

        for (int i = 0; i < n; i++)
        {
            if (a2i.ContainsKey(a[i]))
            {
                i2a.Remove(a2i[a[i]]);
            }
            else
            {
                if (i2a.Count >= k)
                {
                    int j = i2a.First().Key;
                    i2a.Remove(j);
                    a2i.Remove(a[j]);
                    ans = Math.Max(ans, i - done);
                    done = j + 1;
                }
            }
            i2a.Add(i, a[i]);
            a2i[a[i]] = i;
        }

        ans = Math.Max(ans, n - done);
        Console.WriteLine(ans);
    }
}
