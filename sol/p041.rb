n = readline.to_i
z = Array.new(n) do
    Complex(*readline.split.map(&:to_i))
end

min_x = z.map(&:real).min
min_y = z.map(&:imag).min
max_x = z.map(&:real).max
max_y = z.map(&:imag).max

z0 = z[0]
for zi in z
    if zi.imag < z0.imag or (zi.imag == z0.imag && zi.real < z0.real) then
        z0 = zi
    end
end

z.sort! do |zi, zj|
    zi -= z0
    zj -= z0
    if zi.angle != zj.angle then
        zi.angle <=> zj.angle
    else
        zi.abs2 <=> zj.abs2
    end
end

wall = []
for zi in z
    while wall.size >= 2
        zj = wall[-1]
        zk = wall[-2]
        if ((zi - zj) / (zk - zj)).angle < 0 then
            break
        end
        wall.pop
    end
    wall.push(zi)
end
wall.push(z0)

ans = (max_x - min_x + 1) * (max_y - min_y + 1)

for j in 1...(wall.size)
    zi = wall[j - 1]
    zj = wall[j]

    dx = (zj - zi).real.to_i
    dy = (zj - zi).imag.to_i

    ans -= ((dx.abs + 1) * (dy.abs + 1) - (dx.abs.gcd(dy.abs) + 1)) / 2

    if dy > 0 then
        ans -= (max_x - [zi.real, zj.real].max) * dy
    else
        ans -= (min_x - [zi.real, zj.real].min) * dy
    end
end

puts ans - n
