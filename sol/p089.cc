#include <algorithm>
#include <cstdio>
#include <set>
#include <unordered_map>
#include <vector>

#include <atcoder/fenwicktree>
#include <atcoder/modint>

using namespace atcoder;
using namespace std;

using mint = modint1000000007;

int main()
{
    int n;
    long k;
    scanf("%d%ld", &n, &k);

    vector<int> a(n);
    {
        for (int i = 0; i < n; i++) scanf("%d", &a[i]);
        const vector<int> b(a);
        sort(a.begin(), a.end());
        unordered_map<int, int> map(n);
        for (int i = 0; i < n; i++) map.emplace(a[i], i);
        for (int i = 0; i < n; i++) a[i] = map[b[i]];
    }

    fenwick_tree<mint> dp(n + 1);
    dp.add(0, 1);
    fenwick_tree<long> fq(n + 1);

    long count = 0;
    int j = 0;

    for (int i = 0; i < n; i++) {
        count += fq.sum(a[i] + 1, n + 1);
        fq.add(a[i], +1);

        for (; count > k; j++) {
            count -= fq.sum(0 + 0, a[j] + 0);
            fq.add(a[j], -1);
        }

        dp.add(i + 1, dp.sum(j, i + 1));
    }

    printf("%d\n", dp.sum(n, n + 1).val());

    return 0;
}
