def a(n)
    m = n
    while m > 0
        n += m % 10
        m /= 10
    end
    n % 100000
end

def main
    n, k = readline.split.map(&:to_i)
    map = {}
    while k > 0
        if map[n] && k >= map[n] - k then
            k = k % (map[n] - k)
        else
            map[n] = k
            n = a(n)
            k -= 1
        end
    end
    puts n
end

main if __FILE__ == $0
