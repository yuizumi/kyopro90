n, q = readline.split.map!(&:to_i)
a = readline.split.map!(&:to_i)

west = a.dup
east = a.dup

ans = (1...n).inject(0) do |sum, i|
    sum + (east[i-1] - west[i]).abs
end

q.times do
    l, r, v = readline.split.map(&:to_i)
    l -= 1
    if l > 0 then
        ans -= (east[l-1] - west[l]).abs
        west[l] += v
        ans += (east[l-1] - west[l]).abs
    end
    if r < n then
        ans -= (east[r-1] - west[r]).abs
        east[r-1] += v
        ans += (east[r-1] - west[r]).abs
    end

    puts ans
end
