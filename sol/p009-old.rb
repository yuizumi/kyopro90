EPS = 1e-8

n = readline.to_i
z = Array.new(n) do
    x, y = readline.split.map(&:to_i)
    Complex(x, y)
end

ans = 0

for z_i in z
    args = z.reject { |z_j| z_j == z_i }.map do |z_j|
        (z_j - z_i).arg
    end
    args.sort!
    args += args.map { |arg| arg + 2.0 * Math::PI }

    min = args[0]
    min_i = 0
    for max in args
        while max - min > Math::PI + EPS do
            min_i += 1
            min = args[min_i]
        end
        break if min >= Math::PI
        ans = max - min if ans < max - min
    end
end

puts ans / Math::PI * 180.0
